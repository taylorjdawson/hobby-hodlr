const { pubsub, withFilter, EVENTS } = require('../subscriptions')
const { DateTime } = require('luxon')
const Mongo = require('../../modules/mongo')
const { getAccountHoldings, calcAccountTotalsHistorical } = require('../../modules/accounting/accountHoldings')
const { getTypeSeries } = require('../../modules/timeseries')

const getSeriesByType = async (args, type) => {
  if (!args || !args.userId) return Promise.reject('Arguments missing!')
  let res

  try {
    const tmpRes = await getTypeSeries(args.userId, 'balanceSeries')
    res = tmpRes && tmpRes[0] && tmpRes[0].data ? tmpRes.map(d => ({ ...d, ...d.data })) : tmpRes
  } catch (e) {
    // return Promise.reject(e)
    return []
  }

  if (!res) return []

  // If the series has less than 1 month, trigger recalculation, and return backfilled series
  // Ideally this will only get triggered as necessary!
  if (res.length < 31) {
    try {
      res = await calcAccountTotalsHistorical(args.userId)
    } catch (e) {
      // return Promise.reject(e)
      return []
    }
  }

  // allows request for single data assignment, if null returns ALL values
  return res.map(d => {
    let a = { t: DateTime.fromMillis(parseInt(d.timestamp)).toISODate() }
    if (!type) a = { ...d, ...a }
    else a = { ...a, y: d[type] }
    return a
  }) || []
}

module.exports = {
  Query: {
    async accounts(root, args, context, info) {
      if (!args || !args.userId) return Promise.reject('Arguments missing!')
      let res

      try {
        res = await Mongo.find('account', args.userId, args)
      } catch (e) {
        return Promise.reject(e)
      }
      if (!res || res.length <= 0) Promise.resolve([])

      // Get the asset data!
      const finalItems = Promise.all(res.map(async item => {
        if (item.assetId) {
          try {
            item.asset = await Mongo.get('asset', args.userId, { _id: item.assetId })
            item.asset.colors= JSON.stringify(item.asset.colors)
            item.dataType = 'account'
          } catch (e) {
            return Promise.reject(e)
          }
        }
        return item
      }))

      return finalItems || []
    },
    async holdings(root, args, context, info) {
      if (!args || !args.userId) return Promise.reject('Arguments missing!')
      let res

      try {
        res = await getAccountHoldings(args.userId)
      } catch (e) {
        return []
      }
      if (!res || res.length <= 0) return Promise.resolve([])

      // Get the asset data!
      const finalItems = await Promise.all(res.map(async item => {
        if (item.assetId) {
          try {
            item.asset.colors = JSON.stringify(item.asset.colors)
            item.dataType = 'account'
          } catch (e) {
            return Promise.reject(e)
          }
        }
        return item
      }))

      return finalItems || []
    },
    async networthSeries(root, args, context, info) { return getSeriesByType(args, 'networth') },
    async cryptoSeries(root, args, context, info) { return getSeriesByType(args, 'crypto') },
    async traditionalSeries(root, args, context, info) { return getSeriesByType(args, 'traditional') },
    async transactionsSeries(root, args, context, info) { return getSeriesByType(args, 'transactions') },
  },

  Mutation: {
    updateAccount: async (root, data, context) => {
      if (!data || !Object.keys(data).length || !data.userId || !data.assetId || !data.address)
        return Promise.reject('Data missing!')
      let item

      // minor data format cleanup
      if (data && data.meta) data.meta = JSON.parse(data.meta)

      // If i dont have the _id, simply add, otherwise Update
      const method = !data._id ? 'add' : 'update'
      try {
        item = await Mongo[method]('account', data.userId, data)
      } catch (e) {
        return Promise.reject(e)
      }

      // Trigger all listeners
      pubsub.publish(EVENTS.ACCOUNTS_UPDATED, {
        updateUser: item,
      })

      return item
    },

    removeAccount: async (root, data, context) => {
      if (!data || !Object.keys(data).length || !data.id) return Promise.reject('Data missing!')
      let item

      try {
        res = await Mongo.delete('account', data.id)
      } catch (e) {
        return Promise.reject(e)
      }

      return data
    },
  },

  Subscription: {
    updateAccount: {
      subscribe: () => pubsub.asyncIterator(EVENTS.ACCOUNTS_UPDATED),
    },
  },
}
