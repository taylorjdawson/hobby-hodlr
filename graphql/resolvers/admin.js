const { pubsub, withFilter, EVENTS } = require('../subscriptions')
const Mongo = require('../../modules/mongo')

module.exports = {
  Query: {
    async admin(root, args, context, info) {
      let res

      try {
        res = await Mongo.find('admin', null, {})
      } catch (e) {
        return Promise.reject(e)
      }

      return res && res[0] ? res[0] : {}
    },
  },

  Mutation: {
    updateAdmin: async (root, data, context) => {
      if (!data || !Object.keys(data).length) return Promise.reject('Data missing!')
      let item

      try {
        item = await Mongo.update('admin', null, data)
      } catch (e) {
        return Promise.reject(e)
      }

      // Trigger all listeners
      pubsub.publish(EVENTS.ADMIN_UPDATED, { updateAdmin: item, })
      return item
    },
  },

  Subscription: {
    updateAdmin: {
      subscribe: () => pubsub.asyncIterator(EVENTS.ADMIN_UPDATED),
    },
  },
}
