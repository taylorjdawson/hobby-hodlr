const { pubsub, withFilter, EVENTS } = require('../subscriptions')
const Mongo = require('../../modules/mongo')

module.exports = {
  Query: {
    // Endpoint for querying raw assets with pagination
    async assets(root, args, context, info) {
      const filters = args && args.filters ? JSON.parse(args.filters) : {}
      const sort = args.sort || null
      const skip = args.skip || 0
      const limit = args.limit || 100
      const field = args.field || 'name'
      let arrItems = []

      if (args.search) {
        try {
          arrItems = await Mongo.search('asset', field, args.search, { rank: 1, ...filters })
        } catch (e) {
          return Promise.reject(e)
        }
      } else {
        try {
          arrItems = await Mongo.find('asset', filters, { sort, skip, limit })
        } catch (e) {
          return Promise.reject(e)
        }
      }

      return arrItems.map(a => ({ ...a, colors: JSON.stringify(a.colors) })) || []
    },

    // Supports multiple access styles:
    // Params: _id, symbol, search
    async asset(root, args, context, info) {
      if (!args) return Promise.reject('Arguments missing!')
      let res
      let asset = {}

      if (args.id) asset = Mongo.ObjectId(args.id)
      if (args.symbol) asset.symbol = args.symbol
      if (!Object.keys(asset).length) return Promise.reject('Arguments missing!')

      try {
        res = await Mongo.get('asset', asset)
      } catch (e) {
        return Promise.reject(e)
      }

      return res ? { ...res, colors: JSON.stringify(res.colors) } : {}
    },
  },

  Mutation: {
  },

  Subscription: {
    updateAssets: {
      subscribe: () => pubsub.asyncIterator(EVENTS.ASSETS_UPDATED),
    },
  },
}
