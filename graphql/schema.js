const { gql } = require('apollo-server-express')
const account = require('./schemas/account')
const admin = require('./schemas/admin')
const asset = require('./schemas/asset')
const goal = require('./schemas/goal')
const portfolio = require('./schemas/portfolio')
const research = require('./schemas/research')
const user = require('./schemas/user')

const typeDefs = gql`
  ${account.Single}
  ${admin.Single}
  ${asset.Single}
  ${goal.Single}
  ${portfolio.Single}
  ${research.Single}
  ${user.Single}

  type Query {
    ${account.Query}
    ${admin.Query}
    ${asset.Query}
    ${goal.Query}
    ${portfolio.Query}
    ${research.Query}
    ${user.Query}
  }

  type Mutation {
    ${account.Mutation}
    ${admin.Mutation}
    ${asset.Mutation}
    ${goal.Mutation}
    ${portfolio.Mutation}
    ${research.Mutation}
    ${user.Mutation}
  }

  type Subscription {
    ${account.Subscription}
    ${admin.Subscription}
    ${asset.Subscription}
    ${goal.Subscription}
    ${portfolio.Subscription}
    ${research.Subscription}
    ${user.Subscription}
  }
`

module.exports = { typeDefs }
