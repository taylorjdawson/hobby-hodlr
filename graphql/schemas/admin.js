module.exports = {
  Single: `
    type Admin {
      _id: String
      alphavantageId: String
      amberdataId: String
      infuraId: String
      portisDappId: String
      quandlId: String
      dataPollingInterval: Int
      alertingInterval: Int
    }
  `,

  Query: `
    admin: Admin
  `,

  Mutation: `
    updateAdmin(
      _id: String
      alphavantageId: String
      amberdataId: String
      infuraId: String
      portisDappId: String
      quandlId: String
      dataPollingInterval: Int
      alertingInterval: Int
    ): Admin
  `,

  Subscription: `
    updateAdmin: Admin
  `
}
