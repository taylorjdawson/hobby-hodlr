require('dotenv').config()
const axios = require('axios')
const Big = require('big.js')
const { DateTime } = require('luxon')
const Mongo = require('../mongo')
const Timeseries = require('../timeseries')

const getTodayDateMillis = () => DateTime.fromISO(DateTime.local().toISODate()).toMillis()

// NOTE: Commented ones need updating
// TODO: Make this accessible to gql for FE filtering
const supportedChains = {
  ada: require('../blockchains/ada'),
  algo: require('../blockchains/algo'),
  atom: require('../blockchains/atom'),
  bch: require('../blockchains/bch'),
  bsv: require('../blockchains/bsv'),
  btc: require('../blockchains/btc'),
  eth: require('../blockchains/eth'),
  ltc: require('../blockchains/ltc'),
  xtz: require('../blockchains/xtz'),
  zec: require('../blockchains/zec'),
}

const supportsBlockchainTokens = ['eth']

// Returns an account item with calculated price data
const getFormattedAccountItem = (account, asset) => {
  const usdPrice = asset.currentPrice
  const balances = {}

  // totalUnits is always denoted at base level, if an asset has 8 decimal places
  // division or multiplication is handled here for data consistency
  // NOTE: For less confusion, when manualUnits is present, totalUnits is left blank but totalUnitsUSD is calculated
  let totalUnits = 0
  if (account.totalUnits) balances.totalUnitsUSD = new Big(account.totalUnits).times(usdPrice).toFixed(2).valueOf()
  if (account.manualUnits) {
    // unit adjustment for base value
    // if (asset.type === 'cryptocurrency') {
    //   const decimals = asset.decimals || 0
    //   const decExp = `1e${decimals}`
    //   const units = new Big(account.manualUnits).div(decExp).valueOf()
    //   balances.totalUnitsUSD = new Big(units).times(new Big(usdPrice)).toFixed(2).valueOf()
    // } else {
    //   balances.totalUnitsUSD = new Big(account.manualUnits).times(new Big(usdPrice)).toFixed(2).valueOf()
    // }
    balances.totalUnitsUSD = new Big(account.manualUnits).times(usdPrice).toFixed(2).valueOf()
  }

  // Staking is only possible for supported chain assets, only add data when present
  if (account.totalStaked) {
    balances.totalStaked = account.totalStaked
    balances.totalStakedUSD = new Big(account.totalStaked).times(usdPrice).toFixed(2).valueOf()
  }
  if (account.totalPendingRewards) {
    balances.totalPendingRewards = account.totalPendingRewards
    balances.totalPendingRewardsUSD = new Big(account.totalPendingRewards).times(usdPrice).toFixed(2).valueOf()
  }

  return {
    ...account,
    asset,
    ...balances,
  }
}

const getUnsupportedChainBalance = (account, asset) => {
  if (!asset || !asset.currentPrice) return Promise.reject('No asset price found!')
  if (!account || !account.manualUnits) return
  return getFormattedAccountItem(account, asset)
}

// TODO: Support historical balance query!
const getSupportedChainBalance = async (account, asset) => {
  if (!asset || !asset.symbol) return Promise.reject('No asset symbol found!')
  if (!asset.currentPrice) return Promise.reject('No asset price found!')
  if (!account || !account.address) return Promise.reject('No account address found!')
  const sym = asset.symbol.toLowerCase()
  let balances

  if (supportsBlockchainTokens.includes(sym)) {
    try {
      balances = await supportedChains[sym].getMultipleBalances(account.address)
    } catch (e) {
      return Promise.reject(e)
    }

    // No balances found for account
    if (!balances || balances.length <= 0) return []
    const allBalances = await Promise.all(balances.map(async balance => {
      const isMainAcct = balance.address === account.address
      const acct = isMainAcct ? { ...account, ...balance } : { ...balance }

      // Get asset context for symbol
      let tmpAsset
      if (isMainAcct) tmpAsset = asset
      else if (balance.asset && !balance.asset.assetId) {
        try {
          tmpAsset = await Mongo.get(Mongo.DBNAMES.asset, null, { symbol: `${balance.asset.symbol}`.toLowerCase() })
          if (tmpAsset && tmpAsset.assetId) {
            acct._id = tmpAsset._id
            acct.assetId = tmpAsset.assetId
          }
        } catch (e) {
          console.log('Failed to get token asset e', e)
        }
        if (!tmpAsset) return null
      }

      return getFormattedAccountItem(acct, tmpAsset)
    }))

    return allBalances.filter(a => typeof a !== 'undefined')
  } else {
    try {
      balances = await supportedChains[sym].getBalance(account.address)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  // No balances found for account
  if (!balances || !balances.totalUnits) return
  return getFormattedAccountItem({ ...account, ...balances }, asset)
}

// Totals all account balances by types
const getBalanceTotals = accounts => {
  const updatedAt = `${DateTime.local().toMillis()}`
  let networth = 0
  let cash = 0
  let traditional = 0
  let transactions = 0
  let crypto = 0
  let interest = 0
  let interestPending = 0
  let dividends = 0
  let lending = 0

  // no accounts, return 0s on all
  if (!accounts || accounts.length <= 0) {
    return { updatedAt, networth: '0.00', cash: '0.00', traditional: '0.00', transactions: '0.00', crypto: '0.00', interest: '0.00', interestPending: '0.00', dividends: '0.00', lending: '0.00' }
  }

  accounts.forEach(a => {
    if (a.totalUnitsUSD) networth = new Big(a.totalUnitsUSD).plus(networth)
    if (a.totalUnitsUSD && a.type === 'cryptocurrency') crypto = new Big(a.totalUnitsUSD).plus(crypto)
    if (a.totalUnitsUSD && a.type === 'cryptocurrency' && a.subtype === 'stablecoin') cash = new Big(a.totalUnitsUSD).plus(cash)
    if (a.totalUnitsUSD && a.type === 'security') traditional = new Big(a.totalUnitsUSD).plus(traditional)
    if (a.totalStakedUSD) interest = new Big(a.totalStakedUSD).plus(interest)
    if (a.totalPendingRewardsUSD) interestPending = new Big(a.totalPendingRewardsUSD).plus(interestPending)
  })

  return {
    updatedAt,
    networth: networth.toFixed(2).valueOf(),
    cash: cash.toFixed(2).valueOf(),
    traditional: traditional.toFixed(2).valueOf(),
    transactions: new Big(transactions).toFixed(2).valueOf(),
    crypto: crypto.toFixed(2).valueOf(),
    interest: interest.toFixed(2).valueOf(),
    interestPending: interestPending.toFixed(2).valueOf(),
    dividends: dividends.toFixed(2).valueOf(),
    lending: lending.toFixed(2).valueOf(),
  }
}

const addHoldingSeriesItem = async (userId, holdings) => {
  await Timeseries.updateItem({
    userId,
    timestamp: getTodayDateMillis(),
    data: holdings,
    type: 'holdingSeries',
  })
}

const getAssetsForAccounts = async (userId, accounts) => {
  const assets = {}
  if (!accounts || accounts.length <= 0) return Promise.resolve(assets)

  try {
    // retrieve asset data for each account
    await Promise.all(accounts.map(async account => {
      // Retrieve DB asset, only if not already cached
      if (!assets[account.assetId]) {
        assets[account.assetId] = await Mongo.get(Mongo.DBNAMES.asset, userId, Mongo.ObjectId(account.assetId))
      }
    }))
  } catch (e) {
    // soft fail
  }

  return Promise.resolve(assets)
}

const getAccountBalances = async (account, asset) => {
  let tmpHoldingItems
  try {
    if (asset) {
      if (account.manualUnits) {
        // Manual: calculate holding total based on asset price
        // Supports manual networks as long as we have trade price for asset, this will return null if no price
        tmpHoldingItems = await getUnsupportedChainBalance(account, asset)
      } else if (supportedChains[asset.symbol]) {
        // Auto: retrieve account totals, calculate total based on asset price
        tmpHoldingItems = await getSupportedChainBalance(account, asset)
      }
    }
  } catch (e) {
    // not sure?!
  }

  return Array.isArray(tmpHoldingItems) ? tmpHoldingItems : [tmpHoldingItems]
}

// Returns last calculated account totals
const getAccountHoldings = async (userId) => {
  if (!userId) throw new Error('User ID Required!')
  let accounts
  let assets = {} // quick cache of asset price data
  const holdings = new Map()

  try {
    // Get user accounts
    accounts = await Mongo.find(Mongo.DBNAMES.account, userId, { userId })
  } catch (e) {
    throw e
  }

  if (!accounts || accounts.length <= 0) return Promise.resolve([])

  // retrieve asset data for each account
  assets = await getAssetsForAccounts(userId, accounts)

  try {
    // loop accounts to process price * balance
    for (const idx in accounts) {
      const asset = assets[accounts[idx].assetId]
      const account = accounts[idx]
      const tmpHoldingItems = await getAccountBalances(account, asset)

      for (const tmpHoldingItem of tmpHoldingItems) {
        if (tmpHoldingItem && tmpHoldingItem.asset && tmpHoldingItem.asset.symbol) {
          if (holdings.has(tmpHoldingItem.asset.symbol)) {
            // DO MATHS
            const tmpAcctItem = holdings.get(tmpHoldingItem.asset.symbol)

            if (tmpAcctItem.totalUnits) tmpHoldingItem.totalUnits = new Big(tmpHoldingItem.totalUnits).plus(tmpAcctItem.totalUnits).valueOf()
            if (tmpAcctItem.manualUnits) tmpHoldingItem.manualUnits = new Big(tmpHoldingItem.manualUnits).plus(tmpAcctItem.manualUnits).valueOf()
            if (tmpAcctItem.totalUnitsUSD) tmpHoldingItem.totalUnitsUSD = new Big(tmpHoldingItem.totalUnitsUSD).plus(tmpAcctItem.totalUnitsUSD).valueOf()
          }
          holdings.set(tmpHoldingItem.asset.symbol, tmpHoldingItem)
        }
      }
    }
  } catch (e) {
    Promise.reject(e)
  }

  return Promise.resolve([...holdings.values()])
}

// Returns latest calculated holdings
const getAccountHoldingsCache = async (userId) => {
  if (!userId) throw new Error('User ID Required!')
  let accounts
  let assets = {} // quick cache of asset price data
  let holdings = []

  try {
    // Get user holdings
    // Gets last 2 holdings items, return 1 array to request
    const now = DateTime.local()
    const endDate = now.toMillis()
    const startDate = now.minus({ days: 1 }).toMillis()
    const tmpHoldings = await Mongo.find(Mongo.DBNAMES.holdingSeries, userId, { userId, timestamp: { $lt: endDate, $gte: startDate } })
    holdings = tmpHoldings && tmpHoldings.length > 0 ? tmpHoldings[0].data || [] : []
  } catch (e) {
    Promise.reject(e)
  }

  // TODO: no accounts would mean no holdings, need to check this!

  // No cache available! Return default computed on the fly holdings, if any
  if (!holdings || holdings.length <= 0) {
    // if we dont have holdings, may as well use this opportunity to compute and store!
    try {
      holdings = await getAccountHoldings(userId)
      if (holdings.length > 0) await addHoldingSeriesItem(userId, holdings)
    } catch (e) {
      // nothing to fail here
    }
    return Promise.resolve(holdings || [])
  }

  try {
    // if we have holdings, get the latest asset prices and apply
    assets = await getAssetsForAccounts(userId, holdings)
  } catch (e) {
    // nothing to fail here
  }

  // Loop holdings and update asset data
  if (holdings && holdings.length > 0) holdings.map(h => {
    if (h.assetId && assets[h.assetId]) h.asset = assets[h.assetId]
    return h
  })

  // return final stuffz
  return Promise.resolve(holdings)
}

// Calculates total balances for user accounts, stores updates for historical use
const calcAccountHoldings = async userId => {
  const holdings = await getAccountHoldings(userId)
  if (!holdings || holdings.length < 0) return Promise.resolve()

  // update user account balances for faster cache
  if (holdings.length > 0) await addHoldingSeriesItem(userId, holdings)
}

// Calculates total currency amounts for user accounts, stores series for historical use
const calcAccountTotals = async userId => {
  const holdings = await getAccountHoldingsCache(userId)
  if (!holdings || holdings.length < 0) return Promise.resolve()
  let balanceTotals

  // store accounts totals in DB (on user)
  // TODO: this needs to be able to recalculate upon an account getting removed/added
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  try {
    // update user account
    balanceTotals = getBalanceTotals(holdings)
    await Mongo.update(Mongo.DBNAMES.user, userId, { _id: userId, totals: balanceTotals })
  } catch (e) {
    Promise.reject(e)
  }

  if (!balanceTotals) return Promise.resolve()

  // add latest balances to users timeseries data
  await Timeseries.upsertItem({
    userId,
    timestamp: getTodayDateMillis(),
    data: balanceTotals,
    type: 'balanceSeries',
  })
}

// Calculates total currency amounts for user accounts, for each date missing from DB
// NOTE: This is just for back-filling data, until transactions are tracked!
const calcAccountTotalsHistorical = async (
  userId,
  start = DateTime.fromISO(DateTime.local().minus({ days: 31 }).toISODate()).toMillis(),
  end = DateTime.local().toMillis()
) => {
  const holdings = await getAccountHoldingsCache(userId)
  if (!holdings || holdings.length < 0) return Promise.resolve()
  const startDate = DateTime.fromMillis(start).minus({ days: 1 }).toISODate()
  const endDate = DateTime.fromMillis(end).toISODate()
  const totalDays = DateTime.fromISO(endDate).diff(DateTime.fromISO(startDate), 'days').toObject().days

  // Generate the last months worth of dates
  // Examples: ['1597906800000']
  const missingDates = []

  // 1. Get historical totals, so we know missing dates needed
  let historicalTotals = await Timeseries.getTypeSeries(userId, 'balanceSeries')
  historicalTotals = historicalTotals.map(d => ({ ...d, ...d.data }))

  // Loop to find the missing timestamps
  const historicalDates = historicalTotals.map(ht => ht.timestamp)
  for (var i = 0; i < totalDays; i++) {
    const tmpDate = DateTime.fromISO(DateTime.fromISO(startDate).plus({ days: i }).toISODate()).toMillis()
    if (!historicalDates.includes(tmpDate)) missingDates.push(tmpDate)
  }

  // 2. Get historical prices for each account holdings, between start/end date
  const prices = {
    // '1597906800000': { btc: '9434.347733118342', ... }
  }
  const heldAssetIds = holdings.map(h => h.asset.assetId)
  try {
    // Parallel FTW!
    await Promise.all(heldAssetIds.map(async aId => {
      // NOTE: This assumes all the historical prices exist for held assets!
      const assetPrices = await Timeseries.getTypeSeries(null, 'assetSeries', aId)
      assetPrices.forEach(ap => {
        prices[ap.timestamp] = prices[ap.timestamp] || {}
        if (ap.data && ap.data.symbol && ap.data.price) {
          prices[ap.timestamp][ap.data.symbol] = ap.data.price || '0'
        }
      })
    }))
  } catch (e) {
    return Promise.reject('Could not get price for historical asset')
  }

  // 3. Sum each days totals, store in DB
  await Promise.all(missingDates.map(async day => {
    const tmpAccountsForSingleDay = []
    if (prices[`${day}`] && prices[`${day}`]) {
      // get holding item for corresponding day
      await Promise.all(holdings.map(async heldItem => {
        if (prices[`${day}`][heldItem.asset.symbol]) {
          let tmpHoldingItems

          if (!heldItem.totalUnitsUSD) tmpHoldingItems = await getAccountBalances(heldItem, heldItem.asset)
          else tmpHoldingItems = [{
            ...heldItem,
            totalUnitsUSD: heldItem.manualUnits
              ? new Big(heldItem.manualUnits).mul(prices[`${day}`][heldItem.asset.symbol]).toFixed(2)
              : new Big(heldItem.totalUnits || 0).mul(prices[`${day}`][heldItem.asset.symbol]).toFixed(2)
          }]

          for (const tmpHoldingItem of tmpHoldingItems) {
            tmpAccountsForSingleDay.push({ ...tmpHoldingItem, day })
          }
        }
      }))
    }

    // add historical balance item to users timeseries data
    const itemTotals = getBalanceTotals(tmpAccountsForSingleDay)
    const historicalItem = {
      userId,
      timestamp: `${day}`,
      data: itemTotals,
      type: 'balanceSeries',
    }

    await Timeseries.upsertItem(historicalItem)
    historicalTotals.push({ ...historicalItem, ...itemTotals })
  }))

  // return fully backfilled data
  return historicalTotals.sort((a, b) => (parseInt(b.timestamp) - parseInt(a.timestamp)))
}

// Get all users and trigger holdings calc
const calcUsersBalanceType = async type => {
  let users

  try {
    // Get all users
    users = await Mongo.find(Mongo.DBNAMES.user, null, {})
  } catch (e) {
    Promise.reject(e)
  }

  if (!users || users.length < 0) return

  try {
    // trigger holdings calc
    for (const user of users) {
      if (type === 'holdings') await calcAccountHoldings(`${user._id}`)
      else await calcAccountTotals(`${user._id}`)
    }
  } catch (e) {
    Promise.reject(e)
  }
}

module.exports = {
  calcUsersBalanceType,
  calcAccountHoldings,
  getAccountHoldings,
  calcAccountTotalsHistorical,
}
