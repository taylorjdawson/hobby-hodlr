require('dotenv').config()
const slack = require('../slack.js')
const rocketchat = require('../rocketchat.js')
// const { getCacheByName } = require('../accounting/externalAssets.js')
const accountQl = require('../../graphql/resolvers/account.js')
const Engine = require('rules-js')
const rules = require('./alertRules.json')
const accountRules = require('./accountRules.json')
const engine = new Engine()

const addCommas = x => {
  if (!x) return 0
  const tmp = x.toString().split('.')
  tmp[0] = tmp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return tmp.join('.')
}

const displayAsCurrency = value => {
  if (!value || isNaN(value)) return '-'
  let decimals = 1 - Math.floor(Math.log(value) / Math.log(10))
  if (!decimals || decimals < 2) decimals = 2
  if (decimals > 8) decimals = 8
  return `$${addCommas(parseFloat(value).toFixed(decimals))}`
}

const formatPercent = value => {
  const p = value.split('.')
  return `${p[0]}.${p[1].substring(0, 2)}%`
}

const checkGreaterThan = (fact, key, amount, lessThan) => {
  if (lessThan && fact[key] && (parseFloat(fact[key]) > amount && parseFloat(fact[key]) < lessThan)) return fact
  if (fact[key] && parseFloat(fact[key]) > amount) return fact
	return
}

const messagePercentChange = (fact, key, timeName) => {
  if (!fact || !fact.currentPrice) return
  const direction = `${fact[key]}`.search('-') === -1
  const percent = `${formatPercent(fact[key])}`
  const price = `${displayAsCurrency(fact.currentPrice)}`

  slack.send({
    active: true,
    text: `${fact.name} (${fact.symbol.toUpperCase()}) is ${direction ? 'up' : 'down'} ${percent} to ${price} in the past ${timeName}.`
  })

  rocketchat.send({
    active: true,
    text: `${fact.name} (${fact.symbol.toUpperCase()}) is ${direction ? 'up' : 'down'} ${percent} to ${price} in the past ${timeName}.`
  })

	return fact
}

const messageStakeChange = fact => {
  if (!fact || !fact.totalStaked) return

  slack.send({
    active: true,
    text: `${fact.name} (${fact.symbol.toUpperCase()}) reward balance is ${fact.totalStaked} and ready to claim!`
  })

  rocketchat.send({
    active: true,
    text: `${fact.name} (${fact.symbol.toUpperCase()}) reward balance is ${fact.totalStaked} and ready to claim!`
  })

	return fact
}

engine.closures.add('fivePercentChangeHourly', fact => {
	return checkGreaterThan(fact, 'changeInPriceHourly', 5, 10)
})

engine.closures.add('tenPercentChangeHourly', fact => {
  return checkGreaterThan(fact, 'changeInPriceHourly', 10)
})

engine.closures.add('fivePercentChangeDaily', fact => {
	return checkGreaterThan(fact, 'changeInPriceDaily', 5, 10)
})

engine.closures.add('tenPercentChangeDaily', fact => {
  return checkGreaterThan(fact, 'changeInPriceDaily', 10)
})

engine.closures.add('greaterThanTenStaked', fact => {
  return checkGreaterThan(fact, 'totalStaked', 10)
})

engine.closures.add('alertPercentChangeHourly', fact => {
  return messagePercentChange(fact, 'changeInPriceHourly', 'hour')
})

engine.closures.add('alertPercentChangeDaily', fact => {
  return messagePercentChange(fact, 'changeInPriceDaily', '24 hours')
})

engine.closures.add('alertStakeClaimReward', fact => {
  return messageStakeChange(fact)
})

engine.add(rules)
engine.add(accountRules)

// evaluate facts using the engine
// REF: https://github.com/bluealba/rules-js
module.exports = {
  runPriceAlerts: async () => {
    // const assets = await getCacheByName('assets')
    // const goals = await getCacheByName('goals')
    // const holdings = await accountQl.Query.holdings()
    // const goalSymbols = goals && goals.length ? goals.map(g => g.symbol) : []
    // const checkSymbols = [].concat(goalSymbols)
    //
    // holdings.forEach(h => {
    //   if (!goalSymbols.includes(h.symbol)) checkSymbols.push(h.symbol)
    // })
    //
    // // Loop all assets
    // await Promise.all(Object.keys(assets).map(async k => {
    //   // Check goals/accounts and filter to that?
    //   if (!checkSymbols.includes(k)) return Promise.resolve()
    //   await engine.process('alert-rules', assets[k])
    // }))
    //
    // return
  },
  runAccountAlerts: async () => {
    // const holdings = await accountQl.Query.holdings()
    //
    // // Loop all holdings and process staked
    // await Promise.all(holdings.map(async h => {
    //   await engine.process('account-rules', h)
    // }))
    //
    // return
  },
  lowBalance: data => {
    // const bal = parseInt(data.balance, 10) / 1e18
    // const value = data.price && data.price.balance && data.price.balance.total ? parseFloat(data.price.balance.total).toFixed(4) : 0
    //
    // slack.send({
    //   active: true,
    //   icon: ':rotating_light:',
    //   text: `Low balance of ${bal} ($${value}) for account ${data.address}! Please validate and refill!`
    // })
    //
    // rocketchat.send({
    //   active: true,
    //   icon: ':rotating_light:',
    //   text: `Low balance of ${bal} ($${value}) for account ${data.address}! Please validate and refill!`
    // })
    //
    // return
  }
}
