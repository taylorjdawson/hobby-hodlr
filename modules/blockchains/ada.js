const axios = require('axios')

const getBalance = async address => {
  const baseUrl = `https://api.blockchair.com/cardano/raw/address`
  let balance = 0
  let staked = 0
  let available = 0
  let rewards = 0
  let delegation = 0

  try {
    const { data } = await axios.get(`${baseUrl}/${address}`)
    if (data && data.data) {
      const res = data && data.data && data.data[address] ? data.data[address] : {}
      if (res.address && res.address.caBalance && res.address.caBalance.getCoin) available = parseFloat(res.address.caBalance.getCoin)
    }
  } catch (e) {
    console.log('getAdaBalance e', `${e}`.substring(0, 100))
  }

  // TODO: Get delegation!
  // Fun maths! :D
  balance = delegation + available

  return {
    totalUnits: balance,
    totalStaked: staked,
    totalPendingRewards: rewards,
  }
}

module.exports = {
  getBalance,
}
