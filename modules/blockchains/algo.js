const axios = require('axios')

// Example:
// {
//   "round": 8463369,
//   "address": "HWV437344G63HDPWGVAJFLK35XZEGSPCQMUVPYKEB4EZOIUQJD7KUVWUTQ",
//   "amount": 9067037939,
//   "pendingrewards": 8432998,
//   "amountwithoutpendingrewards": 9058604941,
//   "rewards": 62483490,
//   "status": "Offline",
//   "balance": 9067037939,
//   "numbertransactions": 6
// }
const getBalance = async address => {
  const baseUrl = `https://api.algoexplorer.io/v1/account`
  let balance = 0
  let staked = 0
  let available = 0
  let rewards = 0
  let delegation = 0

  try {
    const aRes = await axios.get(`${baseUrl}/${address}`)
    let tmpAvailable = 0

    if (aRes && aRes.data) {
      available = aRes.data.balance / 1e6
      // NOTE: Any balance is "staked" until moved
      delegation = aRes.data.amountwithoutpendingrewards / 1e6
      rewards = aRes.data.pendingrewards / 1e6
    }
  } catch (e) {
    console.log('aRes e', e)
  }

  return {
    totalUnits: balance,
    totalStaked: delegation,
    totalPendingRewards: rewards,
  }
}

module.exports = {
  getBalance,
}
