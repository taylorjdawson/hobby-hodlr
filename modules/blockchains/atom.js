const axios = require('axios')

const getBalance = async address => {
  const baseUrl = `https://api.cosmostation.io/v1/account`
  let balance = 0
  let staked = 0
  let available = 0
  let rewards = 0
  let delegation = 0
  let undelegation = 0

  try {
    const aRes = await axios.get(`${baseUrl}/balance/${address}`)
    let tmpAvailable = 0

    if (aRes && aRes.data && aRes.data.result) aRes.data.result.forEach(a => {
      tmpAvailable += parseInt(a.amount, 10)
    })

    // convert from 'uatom'
    available = tmpAvailable / 1e6
  } catch (e) {
    console.log('aRes e', e)
  }

  try {
    const cRes = await axios.get(`${baseUrl}/delegations/${address}`)
    let tmpDelegation = 0
    let tmpRewards = 0

    if (cRes && cRes.data) cRes.data.forEach(r => {
      if (r.balance) tmpDelegation += parseInt(r.balance, 10)

      if (r.delegator_rewards) r.delegator_rewards.forEach(s => {
        tmpRewards += parseFloat(s.amount, 10)
      })
    })

    // convert from 'uatom'
    delegation = tmpDelegation / 1e6
    rewards = tmpRewards / 1e6
  } catch (e) {
    console.log('cRes e', e)
  }

  try {
    const dRes = await axios.get(`${baseUrl}/unbonding-delegations/${address}`)
    let tmpUnDelegation = 0

    if (dRes && dRes.data && dRes.data.entries) dRes.data.entries.forEach(e => {
      if (e.balance) tmpUnDelegation += parseInt(e.balance, 10)
    })

    // convert from 'uatom'
    undelegation = tmpUnDelegation / 1e6
  } catch (e) {
    console.log('dRes e', e)
  }

  // Fun maths! :D
  balance = delegation + undelegation + available
  staked = rewards

  return {
    totalUnits: balance,
    totalStaked: delegation,
    totalUnStaked: undelegation,
    totalPendingRewards: rewards,
  }
}

module.exports = {
  getBalance,
}
