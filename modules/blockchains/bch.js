const axios = require('axios')

const getBalance = async address => {
  const baseUrl = `https://api.blockchair.com/bitcoin-cash/dashboards/address`
  let balance = 0
  let staked = 0
  let available = 0
  let rewards = 0
  let delegation = 0

  try {
    const { data } = await axios.get(`${baseUrl}/${address}`)
    if (data && data.data) {
      const res = data && data.data && data.data[address] ? data.data[address] : {}
      if (res.address && res.address.balance && res.address.balance) available = parseFloat(res.address.balance) / 1e8
    }
  } catch (e) {
    console.log('getBchBalance e', `${e}`.substring(0, 100))
  }

  // Fun maths! :D
  balance = delegation + available

  return {
    totalUnits: balance,
    totalStaked: staked,
    totalPendingRewards: rewards,
  }
}

module.exports = {
  getBalance,
}
