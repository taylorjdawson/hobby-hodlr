const axios = require('axios')

const getMultipleBalances = async address => {
  // https://api.blockchair.com/ethereum/dashboards/address/${address}?erc_20=true
  const baseUrl = `https://api.blockchair.com/ethereum/dashboards/address`
  const balances = []

  try {
    const { data } = await axios.get(`${baseUrl}/${address}?erc_20=true`)
    if (data && data.data) {
      const res = data && data.data && data.data[address] ? data.data[address] : {}

      // get main ETH balance
      if (res.address && res.address.balance && res.address.balance) {
        balances.push({
          address,
          totalUnits: parseFloat(res.address.balance) / 1e18,
        })
      }

      // get ERC balances
      if (res.layer_2 && res.layer_2.erc_20 && res.layer_2.erc_20.length > 0) {
        for (const erc of res.layer_2.erc_20) {
          const totalUnits = parseFloat(erc.balance) / `1e${erc.token_decimals || 0}`
          if (totalUnits > 0) balances.push({
            totalUnits,
            type: 'cryptocurrency',
            address: erc.token_address,
            asset: {
              address: erc.token_address,
              name: erc.token_name,
              symbol: erc.token_symbol,
              decimals: erc.token_decimals || 0,
            },
          })
        }
      }
    }
  } catch (e) {
    console.log('getEthBalances e', `${e}`.substring(0, 100))
  }

  return balances
}

module.exports = {
  getMultipleBalances,
}
