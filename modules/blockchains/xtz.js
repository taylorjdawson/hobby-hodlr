const axios = require('axios')

const getBalance = async account => {
  const url = `https://node1.sg.tezos.org.sg/explorer/account/${account.address}`
  const assets = {}
  let balance = 0
  let staked = 0
  let available = 0
  let rewards = 0
  let delegation = 0

  try {
    const { data } = await axios.get(url, {})
    if (data && data.total_balance) balance = parseFloat(data.total_balance)
    if (data && data.frozen_rewards) staked = parseFloat(data.frozen_rewards)
  } catch (e) {
    console.log('XTZ ERROR', e)
  }

  return {
    totalUnits: balance,
    totalStaked: staked,
    totalPendingRewards: rewards,
  }
}

module.exports = {
  getBalance,
}
