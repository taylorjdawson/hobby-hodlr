require('dotenv').config()
const { getAllAssetsRanking, getUsersAssetPrices, getAssetPricesHistorical } = require('./pricefeed')
const { calcUsersBalanceType } = require('../accounting/accountHoldings')
const alerts = require('../alerting/index')
const { pubsub, withFilter, EVENTS } = require('../../graphql/subscriptions')

const MINUTE = 60 * 1000

const asyncSetTimeout = async ms => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

const pollUntil = async (fn, delay, offset = 10) => {
  let cancelled = false
  const pollRecursive = async () => {
    if (cancelled) return
    fn()
    await asyncSetTimeout(delay)
    pollRecursive()
  }

  setTimeout(() => {
    const p = pollRecursive()
    p.cancel = () => (cancelled = true)
  }, offset)
}

// Responsible for checking data every X time, loading from different sources and aggregating them
class DataAggregator {
  constructor(id) {
    return this
  }

  start() {
    // start polling loop, with config
    pollUntil(this.standardPrices, (30 * MINUTE), 5 * MINUTE)
    pollUntil(this.userPrices, (5 * MINUTE))

    // Users account history
    // TODO: Change to DB settings!
    pollUntil(this.userHoldings, (60 * MINUTE), 1 * MINUTE)
    pollUntil(this.userTotals, (5 * MINUTE), 2 * MINUTE)

    // TESTING::::::::::
    // getAssetPricesHistorical()

    // // process alerts every hour
    // pollUntil(this.runPriceAlerts, parseInt(process.env.EXTERNAL_ALERTING_INTERVAL, 10) || (60 * 60 * 1000))
    //
    // // process alerts every 5 minutes
    // pollUntil(this.runAccountAlerts, 5 * 60 * 1000)
    //
    // // process alerts every 5 minutes
    // pollUntil(this.runAccountBalanceChecks, 1 * 60 * 1000)
  }

  standardPrices() {
    getAllAssetsRanking()
  }

  userPrices() {
    getUsersAssetPrices()
  }

  userHoldings() {
    calcUsersBalanceType('holdings')
  }

  userTotals() {
    calcUsersBalanceType('totals')
  }

  // runPriceAlerts() {
  //   alerts.runPriceAlerts()
  // }
  //
  // runAccountAlerts() {
  //   alerts.runAccountAlerts()
  // }
  //
  // async runAccountBalanceChecks() {
  //   const address = process.env.EXTERNAL_LOWBALANCE_ADDRESS || ''
  //   const data = await checkLowBalance(address)
  //
  //   if (data) alerts.lowBalance({ ...data, address })
  // }

  stop() {
    pollUntil.cancel()
  }
}

module.exports = new DataAggregator()
