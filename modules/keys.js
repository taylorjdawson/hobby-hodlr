require('dotenv').config()
const fs = require('fs')
const path = require('path')

const keyPath = process.env.DOCKER === 'true' ? "/run/secrets/" : "./"

const privateKey = fs.readFileSync(path.join(keyPath, "jwtRS256.key"))
const publicKey = fs.readFileSync(path.join(keyPath, "jwtRS256.key.pub"))

module.exports = {
  privateKey,
  publicKey,
}
