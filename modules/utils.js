const fs = require('fs')
const path = require('path')
const { DateTime } = require('luxon')

const getJsonFile = async filePath => {
  const jsonPath = path.join(__dirname, `${filePath}.json`)

  try {
    const rawJson = await fs.readFileSync(jsonPath, 'utf-8')
    return JSON.parse(rawJson)
  } catch (e) {
    return { err: e }
  }
}

const saveJsonFile = async (filePath, data) => {
  try {
    // save data
    await fs.writeFileSync(filePath, JSON.stringify(data))
  } catch (e) {
    return Promise.reject(`Could not save ${filePath}`, e.message)
  }
  // console.log('Saved File:', filePath)
  return Promise.resolve(data)
}

const getTodayDateMillis = () => DateTime.fromISO(DateTime.local().toISODate()).toMillis()

const isDateBetween = ({ startDate, endDate, date }) => {
  const s = DateTime.fromISO(startDate)
  const e = DateTime.fromISO(endDate)
  const d = DateTime.fromISO(date)
  return (s.startOf('day') < d.startOf('day')) && (e.startOf('day') > d.startOf('day'))
}

const getAssetIdBySymbol = async (symbol, file) => {
  const all = await getJsonFile(file)
  let assetId

  all.forEach(a => {
    if (a.symbol && a.symbol.toLowerCase() === symbol.toLowerCase()) {
      assetId = a.assetId
    }
  })

  return assetId
}

const getImageFileType = src => {
  const ext = src ? src.split('.').pop() : null
  return ext ? ext.split('?')[0] : 'png'
}

const getLogoName = asset => {
  const type = asset.fileType || getImageFileType(asset.src)
  return `${asset.name.replace(/ /g, '_').replace(/\//g, '').toLowerCase()}_${asset.symbol.toLowerCase()}.${type}`
}

module.exports = {
  getJsonFile,
  saveJsonFile,
  getAssetIdBySymbol,
  getLogoName,
  getImageFileType,
  getTodayDateMillis,
  isDateBetween,
}
