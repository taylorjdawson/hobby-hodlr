export const Admin = `{
    _id
    alphavantageId
    amberdataId
    infuraId
    portisDappId
    quandlId
    dataPollingInterval
    alertingInterval
  }`

export const GetAdmin = `query GetAdmin{
  admin ${Admin}
}`

export const UpdateAdmin = `mutation UpdateAdmin(
  $_id: String
  $alphavantageId: String
  $amberdataId: String
  $infuraId: String
  $portisDappId: String
  $quandlId: String
  $dataPollingInterval: Int
  $alertingInterval: Int
) {
  updateAdmin(
    _id: $_id
    alphavantageId: $alphavantageId
    amberdataId: $amberdataId
    infuraId: $infuraId
    portisDappId: $portisDappId
    quandlId: $quandlId
    dataPollingInterval: $dataPollingInterval
    alertingInterval: $alertingInterval
  ) ${Admin}
}`

export const UpdateAdminEvent = `subscription UpdateAdmin {
  updateAdmin ${Admin}
}`
