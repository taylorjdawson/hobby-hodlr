export default {
  initialized: state => state.initialized,
  authenticated: state => state.authenticated,
  rememberlogin: state => state.rememberlogin,
  token: state => state.token,
  username: state => state.username,
  user: state => state.user,

  adminId: state => state.adminId,
  alphavantageId: state => state.alphavantageId,
  amberdataId: state => state.amberdataId,
  infuraId: state => state.infuraId,
  portisDappId: state => state.portisDappId,
  quandlId: state => state.quandlId,
}
