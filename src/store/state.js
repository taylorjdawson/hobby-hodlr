export default {
  initialized: false,
  authenticated: false,
  rememberlogin: null,
  token: null,
  username: null,
  user: {},

  // API Key Modules:
  adminId: null,
  alphavantageId: null,
  amberdataId: null,
  infuraId: null,
  portisDappId: null,
  quandlId: null,
}
